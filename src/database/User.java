/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package database;

/**
 *
 * @author Admin
 */
public class User {
    int userId;
    String loginName;
    String password;
    String name;
    String sername;
    int typeId;

    public User() {
        this.userId = -1;
    }

    public User(int userId, String loginName, String password, String name, String sername, int typeId) {
        this.userId = userId;
        this.loginName = loginName;
        this.password = password;
        this.name = name;
        this.sername = sername;
        this.typeId = typeId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSername() {
        return sername;
    }

    public void setSername(String sername) {
        this.sername = sername;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    @Override
    public String toString() {
        return "User{" + "userId=" + userId + ", loginName=" + loginName + ", password=" + password + ", name=" + name + ", sername=" + sername + ", typeId=" + typeId + '}';
    }
    
}
